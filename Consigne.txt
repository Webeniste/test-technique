Test technique pour tester le niveau et la capacité à apprendre en autonomie

1) En django rest framework:

Réaliser une API (crud) de gestion de projets:
 - Créer un projet
 - Lister les projets
 - Supprimer un projet
 - Modifier un projet

Un projet est défini par :
 - Un nom
 - Une description

2) Réaliser un application react qui :
 - Affiche la liste des projets
 - Le détail d'un projet (en cliquant dessus)


Mettre le code sous gitlab (ou github).
Réalisation sous docker possible.