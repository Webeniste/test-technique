import React from "react"
import { connect } from 'react-redux'
import { getProjects, deleteProject } from '../Redux/actions.js'
import { List, Button, Typography } from 'antd'
import "antd/dist/antd.css"


class ListProjects extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            projects: []
        }
    }
    
    componentDidMount = () => {
        this.getProjects()
    }
    
    getProjects = () => {
        this.props.getProjects()
    }

    render = () => {
        const projects = this.props.projects
        // console.log(projects)

        return (
            <List
                header={<h2>Liste des projets<hr></hr></h2>}
                footer={<div style={{textAlign: "center"}}><Button onClick={() => this.props.selectProject({})}> Ajouter un projet</Button></div>}
                bordered
                dataSource={projects}
                renderItem={p => (
                    <List.Item actions={[<Button onClick={()=>this.props.selectProject(p)}>+ infos</Button>]} style={{borderBottom: "1px solid lightgrey"}}>
                        { p.id === this.props.currentProject.id ? <Typography.Text mark>{`#${p.id} - ${p.name}`}</Typography.Text> : `#${p.id} - ${p.name}` }
                    </List.Item>
                )}
            />
        )
    }
}

const mapStateToProps = (state) => ({
    projects: state.projects,
})

const mapDispatchToProps = {
    getProjects, deleteProject
}

export default connect(mapStateToProps, mapDispatchToProps)(ListProjects)