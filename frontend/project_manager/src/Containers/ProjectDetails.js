import React from "react"
import { Card } from 'antd'
import { DeleteOutlined } from '@ant-design/icons'
import "antd/dist/antd.css"

const ProjectDetails = ({project, deleteProject}) => {

    console.log("project details for", project)

    return (
        <Card
            key={project.id}
            title={"#" + project.id + " - " + project.name}
            style={{width: '400px'}}
            actions={[
                <DeleteOutlined key="delete" onClick={() => deleteProject(project.id)} />,
            ]}
        >
            <p>{project.description}</p>
        </Card>
    )
}

export default ProjectDetails