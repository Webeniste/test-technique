import React from "react"
import { Form, Input, Button } from 'antd'
import "antd/dist/antd.css"


class CreateNewProject extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      name: null,
      description: null,
    }
  }

  layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  }
  /* eslint-disable no-template-curly-in-string */
  
  validateMessages = {
    required: '${label} is required!',
  }
  /* eslint-disable no-template-curly-in-string */

  onFinish = (values) => {
    console.log(values)
    console.log("Submitting", values.project)
    this.props.createProject(values.project)
  }

  render = () => {
    const projects = this.props.projects
    console.log(projects)

    return (
      <Form {...this.layout} style={{width: "400px"}} name="nest-messages" onFinish={this.onFinish} validateMessages={this.validateMessages}>
        <Form.Item
          name={['project', 'name']}
          label="Name"
          rules={[
            {
              required: true,
            },
            {
              pattern: /^[\w]{0,50}$/,
              message: "Entre 1 et 50 caractères svp",
            }
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name={['project', 'description']} label="Description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item wrapperCol={{ ...this.layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit" >
            Créer
          </Button>
        </Form.Item>
      </Form>
    )   
  }
}

export default CreateNewProject