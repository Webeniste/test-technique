import React from "react"
import ListProjects from "./ListProjects"
import CreateNewProject from "./CreateNewProject"
import ProjectDetails from "./ProjectDetails"
import {Layout, Row, Col, message} from 'antd'
import "antd/dist/antd.css"
import {connect} from 'react-redux'
import { createProject, deleteProject } from "../Redux/actions"


class ProjectManager extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hasSelectedProject: false,
            selectedProject: {}
        }
    }

    selectProject = project => {
        if (project && Object.keys(project).length === 0) {
            // no project
            this.setState({selectedProject: project, hasSelectedProject: false})
        } else {
            this.setState({selectedProject: project, hasSelectedProject: true})
        }
    }

    createOneProject = project => {
        console.log("Create project", project)
        this.props.createProject( project, ()=>message.info('Project created'))
    }

    deleteOneProject = projectId => {
        console.log("Delete project id", projectId)
        this.props.deleteProject( projectId, ()=>message.info('Project deleted'))
        this.selectProject({})
    }

    render = () => {
        const { Header, Content } = Layout

        return (
            <Layout>
                <Header style={ {padding: "50px 50px"} }>
                    <h2 style={ {backgroundColor: "white", textAlign: "center", lineHeight: "2.8rem"} }>
                        Test technique : Affichage de projets (frontend en React et backend en Django Rest)
                    </h2>
                </Header>
                <Content style={ {padding: "50px 50px", background: "#001529"} }>
                    <Layout>
                        <Row gutter={[48, 48]}>
                            <Col span={9}>
                                <ListProjects selectProject={this.selectProject} currentProject={this.state.selectedProject} />
                            </Col>
                            <Col style={{paddingTop: "50px"}}>
                                {this.state.hasSelectedProject === false ? <CreateNewProject createProject={this.createOneProject} /> : <ProjectDetails project={this.state.selectedProject} deleteProject={this.deleteOneProject} /> }
                            </Col>
                        </Row>
                    </Layout>
                </Content>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    hasSelectedProject: state.hasSelectedProject,
    selectedProject: state.selectedProject
})

const mapDispatchToProps = {
    createProject, deleteProject
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectManager)