import Types from "./types"
const initialState = {
    projects: [],
    loading:false
}

const projectReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.PROJECTS_LOADING: {
            console.log("create_item")
            return {...state, loading: action.payload}
        }

        case Types.GET_PROJECTS: {
            return {...state, projects: action.payload}
        }

        case Types.DELETE_PROJECT: {
            return {...state, projects: state.projects.filter(project => project.id !== action.payload)}
        }
        default:
            return state
    }
}

export default projectReducer