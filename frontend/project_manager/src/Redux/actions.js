import Types from "./types"
import axios from "axios"

export const getProjects = () => {
    return dispatch => {
        dispatch({type:Types.PROJECTS_LOADING, payload:true})
        axios.get(`${process.env.REACT_APP_HOST_IP_ADDRESS}/api/projects`)
            .then(response => {
                    dispatch({type:Types.GET_PROJECTS, payload:response.data})
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.PROJECTS_LOADING, payload:false})
            }
            )
    }
}

export const deleteProject = (id,callback) => {
    return dispatch => {
        dispatch({type:Types.PROJECTS_LOADING, payload:true})
        axios.delete(`${process.env.REACT_APP_HOST_IP_ADDRESS}/api/projects/${id}/`)
            .then(response => {
                    dispatch({type:Types.DELETE_PROJECT, payload:id})
                    callback()
                }
            )
            .catch(err => {
                    console.log(err)
                }
            );
    }
}

export const createProject = (data,callback) => {
    return dispatch => {
        axios.post(`${process.env.REACT_APP_HOST_IP_ADDRESS}/api/projects/`, data)
            .then(response => {
                console.log(response)
                    dispatch({type:Types.CREATE_PROJECT, payload:response.data})
                    callback()
                }
            )
            .catch(err => {
                    console.log(err)
                    dispatch({type:Types.PROJECTS_LOADING, payload:false})
                }
            )
    }
}