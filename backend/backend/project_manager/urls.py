from . import views
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register("projects", views.ProjectViewSet, "projects")

urlpatterns = router.urls
